#!/bin/bash
sed -i "s/1/5/g" msh.txt
sed -i "s/2/4/g" msh.txt
sed -i "s/2/3/g" msh.txt
sed -i "s//2/g" msh.txt
sed -i "s/#79ee93/1/g" msh.txt
sed -i "s/#73d9de/0/g" msh.txt
sed -i "s/endline/|END|/g" msh.txt
sed -i ':a;N;$!ba;s/\n//g' msh.txt

#['#73d9de', '#79ee93', '#a3f176', '#dfed75', '* #f0917a', '#f0c677', 'endline']


