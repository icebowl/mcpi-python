#!/bin/bash
sed -i "s/#ffffff/6/g" mandel.txt
sed -i "s/#c6d0c7/5/g" mandel.txt
sed -i "s/#969874/4/g" mandel.txt
sed -i "s/#969491/3/g" mandel.txt
sed -i "s/#64595/2/g" mandel.txt
sed -i "s/#332e2f/1/g" mandel.txt
sed -i "s/#000000/0/g" mandel.txt
sed -i "s/endline/|END|/g" mandel.txt
sed -i ':a;N;$!ba;s/\n//g' mandel.txt

#['#000000', '#332e2f', '#64595c', '#969491', '#969874', '#c6d0c7', '#ffffff', 'endline']

