from mcpi.minecraft import Minecraft
from mcpi import block
from   time import sleep
from random import seed
from random import randint
seed(110)

import math
pi = math.pi
sin = math.sin
cos = math.cos
def init():
 ipString = "127.0.0.1"
 #ipString = "192.168.7.226"
 #mc = Minecraft.create("127.0.0.1", 4711)
 mc = Minecraft.create(ipString, 4711)
 mc.setting("world_immutable",False)
 #mc.setting("world_immutable",True)
 #x, y, z = mc.player.getPos()  
 return mc

def check(mc,x,y,z):
	mc.setBlocks(x,y-10,z,x,y+2,z,35,15)
	mc.setBlocks(x+5,y-10,z+5,x+5,y+2,z+5,35,14)# RED   5,5
	mc.setBlocks(x-5,y-10,z+5,x-5,y+2,z+5,35,5) # GREEN -5,5 
	mc.setBlocks(x-5,y-10,z-5,x-5,y+2,z-5,35,3) # LIGHT BLUE -5, -5
	mc.setBlocks(x+5,y-10,z-5,x+5,y+2,z-5,35,4) # YELLOW 5, -5      
	
	mc.setBlocks(x+35,y-10,z+35,x+35,y+2,z+35,35,14)# RED   5,5
	mc.setBlocks(x-35,y-10,z+35,x-35,y+2,z+35,35,5) # GREEN -5,5 
	mc.setBlocks(x-35,y-10,z-35,x-35,y+2,z-35,35,3) # LIGHT BLUE -5, -5
	mc.setBlocks(x+35,y-10,z-35,x+35,y+2,z-35,35,4) # YELLOW 5, -5      
	
	
'''
 
wool 35,3  LIGHT BLUE 
wool 35,4  YELLOW
wool 35,5  GREEN
wool 35,14  RED

'''

def land(mc):
	AIR,DIRT,FARMLAND,TALLGRASS,GRASS = 0,3,60,31,2
	h = -5
	mc.setBlocks(-127,h-10,-127,128,64,128,AIR)
	mc.setBlocks(-127,h-11,-127,128,h-1,128,DIRT)
	mc.setBlocks(-127,h,-127,128,h,128,FARMLAND)
	mc.setBlocks(-64,h,-64,64,h,64,FARMLAND)
	mc.setBlocks(-64,h+1,-64,64,h+1,64,TALLGRASS)
	mc.setBlocks(-53,h,-53,53,h,53,AIR)
	mc.setBlocks(-53,h,-53,53,h,53,GRASS)
	
def plant(mc):
	'''
	SAPLING               6
	GRASS_TALL           31
	FLOWER_YELLOW        37
	FLOWER_CYAN          38
	MUSHROOM_BROWN       39
	MUSHROOM_RED         40
	CACTUS               81
	SUGAR_CANE           83
	MELON               103
	WOOD                 17
	'''
	mc.setBlocks(-24,0,-1,-26,20,1,17)
	h = -5
	for _ in range(2):
		x = randint(-100, 100)
		z = randint(-100,100)
		if (x < 53 and x > -53):
			continue
		if (z < 53 and z > -53):
			continue
		flower = randint(37,38)
		mc.setBlock(x,h+1,z,flower)
		print(x,z,flower)

def house_roof(mc,x,y,z):
	h,k,l = x,y,z
	hadj, kadj, ladj = 5,5,5
	#mc.setBlocks(h-hadj,k,l-ladj,h+hadj,k+kadj,l+ladj,0)
	hadj, kadj, ladj = 17,-4,17
	
	for n in range (0,17):  
		m = 17 # glass pane 102  wood 17
		if n > 9:
			m = 20  
		mc.setBlocks(h-hadj+n,k+kadj,l-ladj+n,h+hadj-n,k+kadj,l+ladj-n,m)
		kadj = kadj + 1
	hadj, kadj, ladj = 16,-4,16
	for n in range (0,17):  
		m = 0 # glass pane 102  wood 17
		mc.setBlocks(h-hadj+n,k+kadj,l-ladj+n,h+hadj-n,k+kadj,l+ladj-n,m)
		kadj = kadj + 1
	# roof sofits 
	m = 17 # 
	hadj, kadj, ladj,n = 17,-4,17,0
	mc.setBlocks(h-hadj+n,k+kadj,l-ladj+n,h+hadj-n,k+kadj,l+ladj-n,m)
	mc.setBlocks(h-hadj+n+3,k+kadj,l-ladj+n+3,h+hadj-n-3,k+kadj,l+ladj-n-3,0)
	
def house_base(mc,x,y,z):
	h,k,l = x,y,z
	hadj, kadj, ladj = 32,32,32
	#mc.setBlocks(h-hadj-10,k,l-ladj-10,h+hadj+10,k+kadj+50,l+ladj+10,0)
	hadj, kadj, ladj = 32,-4,32
	for n in range (0,7):  
		m = 24
		if n == 6:
			m = 2
			kadj = kadj -    1
		mc.setBlocks(h-hadj+n*2,k+kadj,l-ladj+n*2,h+hadj-n*2,k+kadj,l+ladj-n*2,m)
		kadj = kadj + 1
		#print(n)
				
def sphere(mc,x,y,z,r,inc):
	inc = 5
	for thetaX in range (0,360,inc):
		xRad = thetaX * (pi / 180) 
		for thetaY in range(0,360,inc):
			yRad= thetaY * (pi / 180) 
			print(xRad,yRad)
			h = (r * sin(yRad) * cos(xRad))+ x 
			k = (r * sin(yRad) * sin(xRad))+ y
			l = r * cos(yRad) + z
			mc.setBlock(h,k,l,57)
			#print(x,y,z,h,k,l)

def frame(mc,x,y,z):
	h,k,l = x,y,z
	hadj, kadj, ladj = 15,0,15
	WOOD_PLANKS = 5
	mc.setBlocks(h-hadj,k,l-ladj,h+hadj,k,l+ladj,1)
	mc.setBlocks(h-hadj,k+1,l-ladj,h+hadj,k+10,l+ladj,WOOD_PLANKS)
	# STEPS
	mc.setBlocks(h-4,k,l-ladj-3,h+4,k,l+ladj-2,1)
	mc.setBlocks(h-4,k,l+ladj+3,h+4,k,l-ladj+3,1)
	
	# air for windows
	mc.setBlocks(h-hadj+1,k+1,l-ladj+2,h+hadj-1,k+11,l+ladj-1,0)
	mc.setBlocks(h-1,k+2,l-16,h+1,k+7,l+16,0) 
	mc.setBlocks(h-10,k+4,l-16,h-6,k+6,l+16,0) 
	mc.setBlocks(h+10,k+4,l-16,h+6,k+6,l+16,0) 
	mc.setBlocks(h+10,k+4,l-16,h+6,k+6,l+16,0) 
	mc.setBlocks(h-20,k+4,l-8,h+20,k+8,l-4,0) 
	mc.setBlocks(h-20,k+4,l+8,h+20,k+8,l+4,0) 
	# inner walls
	#mc.setBlocks(h-hadj+1,k+1,l,h+hadj-1,k+10,l,35,1)
	mc.setBlocks(h+5    ,k+1,l,h+hadj-1,k+10,l,35,1)
	mc.setBlocks(h+4 ,k,l-ladj,h+4,k+10,l+ladj,35,3)
	
	# air for inner door
	mc.setBlocks(h+8,k+1,l-2,h+11,k+7,l+2,0 )
	mc.setBlocks(h+3,k+1,l-4,h+5,k+7,l-8,0 )
	mc.setBlocks(h+3,k+1,l+4,h+5,k+7,l+8,0 )
    #mc.setBlocks(h-5,k+1,l-5,h-9,k+7,l-9,35,2 )
  #  mc.setBlocks(h+8,k+1,l-2,h+11,k+7,l+2,0 )
    
    
def main():
	mc = init()
	mc.player.setPos(0,0,0)  
	x,y,z = mc.player.getPos()
	land(mc)
	plant(mc)
	#createSphere(10,mc)  
	#sphere(mc,x,y,z,5,1)
	house_base(mc,x,y,z)
	house_base(mc,x-32,y ,z)
	#frame(mc,x,y+2,z)
	#house_roof(mc,x,y+17,z)
	check(mc,x,y,z)
	mc.player.setPos(x-18,y+3,z+7)
	

if __name__ == "__main__":
	main()

"""
AIR                   0
STONE                 1
GRASS                 2
DIRT                  3
COBBLESTONE           4
WOOD_PLANKS           5
SAPLING               6
BEDROCK               7
WATER_FLOWING         8
WATER                 8
WATER_STATIONARY      9
LAVA_FLOWING         10
LAVA                 10
LAVA_STATIONARY      11
SAND                 12
GRAVEL               13
GOLD_ORE             14
IRON_ORE             15
COAL_ORE             16
WOOD                 17
LEAVES               18
GLASS                20
LAPIS_LAZULI_ORE     21
LAPIS_LAZULI_BLOCK   22
SANDSTONE            24
BED                  26
COBWEB               30
GRASS_TALL           31
WOOL                 35
FLOWER_YELLOW        37
FLOWER_CYAN          38
MUSHROOM_BROWN       39
MUSHROOM_RED         40
GOLD_BLOCK           41
IRON_BLOCK           42
STONE_SLAB_DOUBLE    43
STONE_SLAB           44
BRICK_BLOCK          45
TNT                  46
BOOKSHELF            47
MOSS_STONE           48
OBSIDIAN             49
TORCH                50
FIRE                 51
STAIRS_WOOD          53
CHEST                54
DIAMOND_ORE          56
DIAMOND_BLOCK        57
CRAFTING_TABLE       58
FARMLAND             60
FURNACE_INACTIVE     61
FURNACE_ACTIVE       62
DOOR_WOODw            64
LADDER               65
STAIRS_COBBLESTONE   67
DOOR_IRON            71
REDSTONE_ORE         73
SNOW                 78
ICE                  79
SNOW_BLOCK           80
CACTUS               81
CLAY                 82
SUGAR_CANE           83
FENCE                85
GLOWSTONE_BLOCK      89
BEDROCK_INVISIBLE    95
STONE_BRICK          98
GLASS_PANE          102
MELON               103
FENCE_GATE          107
GLOWING_OBSIDIAN    246
NETHER_REACTOR_CORE 247
"""
